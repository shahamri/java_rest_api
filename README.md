# Final Shopping app project- ready for evaluation

to run the project please set up the database in mysql and update the application.properties file. 

# Running tests on postman
You can try the following:

http://localhost:8080/api/product/add- to add products
POST- { "name": "cap", "availableQuantity": 2, "price": 4.5 } 
Post- { "name": "toothbrush", "availableQuantity": 2, "price": 2.5 } 


http://localhost:8080/api/placeOrder- to place order

{ "orderDescription": "Test Order Description",

 "cartItems": [
     {
         "productId": 1,
         "quantity": 2
     },
     {
         "productId": 2,
         "quantity": 2
     }


 ],
 "customerEmail": "a@gmail.com",
 "customerName": "Amrita Shah"
}

http://localhost:8080/api/product/getAll- to get all products
http://localhost:8080/api/deleteProduct/1- to delete a particular product
And many more functions as you can find in the api documentation in this project folder

The complex business operation involving multiple tables is the place order feature. The tests have also been updated.

# Class diagram
![alt text](classDiagram.png)
